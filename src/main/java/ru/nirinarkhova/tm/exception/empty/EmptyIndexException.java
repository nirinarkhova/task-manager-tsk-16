package ru.nirinarkhova.tm.exception.empty;

public class EmptyIndexException extends Exception{

    public EmptyIndexException() {
        super("Error! Index is empty...");
    }

}
