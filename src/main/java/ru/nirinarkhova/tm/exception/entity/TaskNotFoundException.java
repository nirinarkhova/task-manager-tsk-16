package ru.nirinarkhova.tm.exception.entity;

public class TaskNotFoundException extends  Exception{

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
