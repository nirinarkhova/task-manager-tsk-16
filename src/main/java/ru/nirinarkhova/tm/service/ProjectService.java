package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.IProjectRepository;
import ru.nirinarkhova.tm.api.IProjectService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) throws EmptyIdException {
        if (id == null) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) throws EmptyIdException {
        if (id == null) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project findOneByName(final String name) throws EmptyNameException {
        if (name == null) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByName(final String name) throws EmptyNameException {
        if (name == null) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project findOneByIndex(final Integer index) throws EmptyIndexException {
        if (index == null) throw new EmptyIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project removeOneByIndex(final Integer index) throws EmptyIndexException {
        if (index == null) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, String name, String description) throws EmptyNameException, EmptyIndexException, ProjectNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, String name, String description) throws EmptyNameException, EmptyIdException, ProjectNotFoundException {
        if (id == null) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) throws EmptyIdException, ProjectNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(String name) throws EmptyNameException, ProjectNotFoundException {
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(String id) throws EmptyIdException, ProjectNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(String name) throws EmptyNameException, ProjectNotFoundException {
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) throws EmptyIdException, ProjectNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(String name, Status status) throws EmptyNameException, ProjectNotFoundException {
        if (name == null) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) throws EmptyIndexException, ProjectNotFoundException {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
