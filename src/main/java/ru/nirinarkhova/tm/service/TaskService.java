package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.ITaskRepository;
import ru.nirinarkhova.tm.api.ITaskService;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) throws EmptyIdException {
        if (id == null) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneById(final String id) throws EmptyIdException {
        if (id == null) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task findOneByName(final String name) throws EmptyNameException {
        if (name == null) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByName(final String name) throws EmptyNameException {
        if (name == null) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task findOneByIndex(final Integer index) throws EmptyIndexException {
        if (index == null) throw new EmptyIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task removeOneByIndex(final Integer index) throws EmptyIndexException {
        if (index == null) throw new EmptyIndexException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, String name, String description) throws EmptyIndexException, TaskNotFoundException, EmptyNameException {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, String name, String description) throws EmptyIdException, TaskNotFoundException, EmptyNameException {
        if (id == null) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) throws EmptyIdException, TaskNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) throws TaskNotFoundException, EmptyNameException {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(String id) throws EmptyIdException, TaskNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) throws TaskNotFoundException, EmptyNameException {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) throws EmptyIdException, TaskNotFoundException {
        if (id == null) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) throws TaskNotFoundException, EmptyNameException {
        if (name == null) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) throws EmptyIndexException, TaskNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
