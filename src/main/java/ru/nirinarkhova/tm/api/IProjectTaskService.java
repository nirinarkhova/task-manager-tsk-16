package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId) throws ProjectNotFoundException;

     Task bindTaskByProject(String taskId, String projectId) throws ProjectNotFoundException, TaskNotFoundException;

     Task unbindTaskFromProject(String taskId) throws TaskNotFoundException;

     Project removeProjectById(String projectId) throws ProjectNotFoundException;

}
