package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description) throws EmptyNameException;

    void add(Task task) throws TaskNotFoundException;

    void remove(Task task) throws TaskNotFoundException;

    void clear();

    Task findOneById(String id) throws EmptyIdException;

    Task findOneByIndex(Integer index) throws EmptyIndexException;

    Task findOneByName(String name) throws EmptyNameException;

    Task removeOneById(String id) throws EmptyIdException;

    Task removeOneByIndex(Integer index) throws EmptyIndexException;

    Task removeOneByName(String name) throws EmptyNameException;

    Task updateTaskById(String id, String name, String description) throws EmptyIdException, TaskNotFoundException, EmptyNameException;

    Task updateTaskByIndex(Integer index, String name, String description) throws EmptyIndexException, TaskNotFoundException, EmptyNameException;

    Task startTaskById(String id) throws EmptyIdException, TaskNotFoundException;

    Task startTaskByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task startTaskByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task finishTaskById(String id) throws EmptyIdException, TaskNotFoundException;

    Task finishTaskByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task finishTaskByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task changeTaskStatusById(String id, Status status) throws EmptyIdException, TaskNotFoundException;

    Task changeTaskStatusByName(String name, Status status) throws TaskNotFoundException, EmptyNameException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws EmptyIndexException, TaskNotFoundException;

}
