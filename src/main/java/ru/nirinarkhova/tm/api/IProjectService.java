package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description) throws EmptyNameException;

    void add(Project project) throws ProjectNotFoundException;

    void remove(Project project) throws ProjectNotFoundException;

    void clear();

    Project findOneById(String id) throws EmptyIdException;

    Project findOneByIndex(Integer index) throws EmptyIndexException;

    Project findOneByName(String name) throws EmptyNameException;

    Project removeOneById(String id) throws EmptyIdException;

    Project removeOneByIndex(Integer index) throws EmptyIndexException;

    Project removeOneByName(String name) throws EmptyNameException;

    Project updateProjectById(String id, String name, String description) throws EmptyNameException, EmptyIdException, ProjectNotFoundException;

    Project updateProjectByIndex(Integer index, String name, String description) throws EmptyNameException, EmptyIndexException, ProjectNotFoundException;

    Project startProjectById(String id) throws EmptyIdException, ProjectNotFoundException;

    Project startProjectByName(String name) throws EmptyNameException, ProjectNotFoundException;

    Project startProjectByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project finishProjectById(String id) throws EmptyIdException, ProjectNotFoundException;

    Project finishProjectByName(String name) throws EmptyNameException, ProjectNotFoundException;

    Project finishProjectByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project changeProjectStatusById(String id, Status status) throws EmptyIdException, ProjectNotFoundException;

    Project changeProjectStatusByName(String name, Status status) throws EmptyNameException, ProjectNotFoundException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws EmptyIndexException, ProjectNotFoundException;

}
