package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.exception.system.IndexIncorrectException;

public interface ITaskController {

    void showList();

    void create() throws TaskNotFoundException, EmptyNameException;

    void clear();

    void showTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void showTaskByName() throws TaskNotFoundException, EmptyNameException;

    void showTaskById() throws TaskNotFoundException, EmptyIdException;

    void removeTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void removeTaskByName() throws TaskNotFoundException, EmptyNameException;

    void removeTaskById() throws TaskNotFoundException, EmptyIdException;

    void updateTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException, EmptyNameException;

    void updateTaskById() throws TaskNotFoundException, EmptyIdException, EmptyNameException;

    void startTaskById() throws TaskNotFoundException, EmptyIdException;

    void startTaskByName() throws TaskNotFoundException, EmptyNameException;

    void startTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void finishTaskById() throws TaskNotFoundException, EmptyIdException;

    void finishTaskByName() throws TaskNotFoundException, EmptyNameException;

    void finishTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

    void changeTaskStatusById() throws TaskNotFoundException, EmptyIdException;

    void changeTaskStatusByName() throws TaskNotFoundException, EmptyNameException;

    void changeTaskStatusByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException;

}
