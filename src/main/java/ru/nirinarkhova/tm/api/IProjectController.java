package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.exception.system.IndexIncorrectException;

public interface IProjectController {

    void showList();

    void create() throws ProjectNotFoundException, EmptyNameException;

    void clear();

    void showProjectByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void showProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void showProjectById() throws ProjectNotFoundException, EmptyIdException;

    void removeProjectByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void removeProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void removeProjectById() throws ProjectNotFoundException, EmptyIdException;

    void  updateProjectByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException, EmptyNameException;

    void  updateProjectById() throws ProjectNotFoundException, EmptyIdException, EmptyNameException;

    void startProjectById() throws ProjectNotFoundException, EmptyIdException;

    void startProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void startProjectByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void finishProjectById() throws ProjectNotFoundException, EmptyIdException;

    void finishProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void finishProjectByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void changeProjectStatusById() throws ProjectNotFoundException, EmptyIdException;

    void changeProjectStatusByName() throws ProjectNotFoundException, EmptyNameException;

    void changeProjectStatusByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

}
