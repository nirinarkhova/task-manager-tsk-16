package ru.nirinarkhova.tm.util;

import ru.nirinarkhova.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER  = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws IndexIncorrectException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
