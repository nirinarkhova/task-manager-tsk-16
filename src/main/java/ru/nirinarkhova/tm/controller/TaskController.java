package ru.nirinarkhova.tm.controller;

import ru.nirinarkhova.tm.api.ITaskController;
import ru.nirinarkhova.tm.api.ITaskService;
import ru.nirinarkhova.tm.enumerated.Sort;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.empty.EmptyIdException;
import ru.nirinarkhova.tm.exception.empty.EmptyIndexException;
import ru.nirinarkhova.tm.exception.empty.EmptyNameException;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.exception.system.IndexIncorrectException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT TYPE]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final  Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void showTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

    @Override
    public void showTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void removeTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void  updateTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException, EmptyNameException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void  updateTaskById() throws TaskNotFoundException, EmptyIdException, EmptyNameException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("[CHANGE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[CHANGE TASK]");
        System.out.println("[ENTER ID:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByName(name, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusByIndex() throws IndexIncorrectException, TaskNotFoundException, EmptyIndexException {
        System.out.println("[CHANGE TASK]");
        System.out.println("[ENTER ID:]");
        Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
    }

}